import './Button.css'

//crée un bouton qui prendra la valeur du label en tant que contenu, et qui viendra chercher la fonction indiquée dans clickHandler au clic

function Button(props) {

    return (
        <button className='buttonComponent' onClick={props.clickHandler}>
            {props.label}
        </button >
    )
}


export default Button

