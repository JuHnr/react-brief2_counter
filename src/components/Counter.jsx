import './Counter.css'

//crée un compteur de participants qui vient chercher la valeur de count 

function Counter(props) {

    return (
        <>
            <h1> Nombre de participants :</h1>
            <h2>{props.count}</h2>
        </>
    )
}

export default Counter