import { useState } from 'react'
import './App.css'
import Button from './components/Button'
import Counter from './components/Counter'

function App() {

  //utilisation de useState pour actualiser le compteur quand nécessaire. Count démarre à 0.
  let [count, setCount] = useState(0);

  //fonction pour incrémenter le compteur
  function handleAdd() {
    setCount(++count)
  }

  //fonction pour décrémenter le compteur
  function handleSubstract() {

    //si le compteur n'est pas à zéro, alors décrémente, sinon ne fait rien
    if (count != 0) {
      setCount(--count)
    }
  }

  //fonction pour réinitialiser le compteur à 0
  function handleReset() {
    setCount(0)
  }

  //ce qui sera affiché dans le DOM
  return (
    <div className='App_container'>

      {/* affiche le compteur avec la valeur de count actuelle */}
      <Counter count={count} />

      {/* affiche les boutons avec la fonction associée à l'événement au clic */}
      <div>
        <Button label="-" clickHandler={handleSubstract} />
        <Button label="+" clickHandler={handleAdd} />
      </div>

      <Button label="RESET" clickHandler={handleReset} />

    </div>
  )
}


export default App
